package bdx.iut.info.constraints;

import bdx.iut.info.amap.constraints.IsContractValidator;
import bdx.iut.info.amap.persistence.domain.Contract;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;
import java.util.regex.Matcher;


public class ConstraintTest {
    private static Validator validator;

    @BeforeClass
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        ConstraintTest.validator = factory.getValidator();
    }

    @Test
    public void testFail() {

        // 1st insertion
        Contract contract = new Contract();
        contract.setTitle("abcd");
        contract.setProducer("fsdf sf");
        contract.setAmount(12);

        Set<ConstraintViolation<Contract>> violations = validator.validate(contract);
        Assert.assertEquals(1, violations.size());
    }

    @Test
    public void testSucceed() {

        // 1st insertion
        Contract contract = new Contract();
        contract.setTitle("[poisson] Contrat de poisson sauvage");
        contract.setProducer("fsdf sf");
        contract.setAmount(12);

        Set<ConstraintViolation<Contract>> violations = validator.validate(contract);
        Assert.assertEquals(0, violations.size());
    }


    @Test
    public void testRegex() {
        Matcher m;

        m = IsContractValidator.p.matcher("invalid");
        Assert.assertFalse(m.matches());

        m = IsContractValidator.p.matcher("[poisson] Contrat de poisson");
        Assert.assertTrue(m.matches());

    }




}
