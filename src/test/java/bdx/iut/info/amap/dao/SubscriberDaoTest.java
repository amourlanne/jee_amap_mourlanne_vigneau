package bdx.iut.info.amap.dao;

import bdx.iut.info.amap.dao.guice.AbstractDaoTest;
import bdx.iut.info.amap.persistence.dao.ContractDao;
import bdx.iut.info.amap.persistence.dao.SubscriberDao;
import bdx.iut.info.amap.persistence.domain.Contract;
import bdx.iut.info.amap.persistence.domain.Subscriber;
import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class SubscriberDaoTest extends AbstractDaoTest {


    @Test
    public void testCreate() {
        final SubscriberDao dao = this.getSubscriberDao();
        final String subscriberId = "toto.titi";

        Subscriber subscriber = new Subscriber();
        subscriber.setId(subscriberId);
        subscriber.setName("toto");
        subscriber.setSurname("titi");
        subscriber.setAddress("toto@machin.fr");

        dao.create(subscriber);

        detach(subscriber); //Remove entity from current session bag
        Subscriber found = reload(Subscriber.class, subscriberId); //try to reload created entity from DB

        Assertions.assertThat(found).isNotNull();
    }


    @Test
    public void testUpdate() {
        final SubscriberDao dao = this.getSubscriberDao();
        final String subscriberId = "toto.titi";

        Subscriber subscriber = new Subscriber();
        subscriber.setId(subscriberId);
        subscriber.setName("toto");
        subscriber.setSurname("titi");
        subscriber.setAddress("toto@machin.fr");

        dao.create(subscriber);
        detach(subscriber); //Remove entity from current session bag

        subscriber = dao.read(subscriberId);
        Assertions.assertThat(subscriber).isNotNull();
        Assertions.assertThat(subscriber.getId()).isEqualTo("toto.titi");
        Assertions.assertThat(subscriber.getName()).isEqualTo("toto");
        Assertions.assertThat(subscriber.getSurname()).isEqualTo("titi");
        Assertions.assertThat(subscriber.getAddress()).isEqualTo("toto@machin.fr");

        subscriber.setName("tutu");
        dao.update(subscriber);
        detach(subscriber); //Remove entity from current session bag

        subscriber = dao.read(subscriberId);
        Assertions.assertThat(subscriber).isNotNull();
        Assertions.assertThat(subscriber.getId()).isEqualTo("toto.titi");
        Assertions.assertThat(subscriber.getName()).isEqualTo("tutu");
        Assertions.assertThat(subscriber.getSurname()).isEqualTo("titi");
        Assertions.assertThat(subscriber.getAddress()).isEqualTo("toto@machin.fr");

        Assert.assertEquals(0, subscriber.numberOfSubscriptions());

    }

    @Test
    public void testDelete() {
        final SubscriberDao dao = this.getSubscriberDao();
        final String subscriberId = "toto.titi";

        Subscriber subscriber = new Subscriber();
        subscriber.setId(subscriberId);
        subscriber.setName("toto");
        subscriber.setSurname("titi");
        subscriber.setAddress("toto@machin.fr");

        dao.create(subscriber);
        detach(subscriber); //Remove entity from current session bag

        List<Subscriber> found = dao.findAll();
        Assertions.assertThat(found).hasSize(1);

        subscriber = reload(Subscriber.class, subscriberId); //Refresh entity in current session

        dao.delete(subscriber);

        found = dao.findAll();
        Assertions.assertThat(found).isEmpty();

    }


    @Test
    public void testContracts() {
        List<Subscriber> subscribers;

        SubscriberDao dao = this.getSubscriberDao();
        ContractDao contractDao = this.getContractDao();

        Subscriber subscriber = new Subscriber();
        subscriber.setId("1");
        subscriber.setName("toto");
        subscriber.setSurname("titi");
        subscriber.setAddress("toto@machin.fr");
        persist(subscriber);

        Contract c = new Contract();
        c.setTitle("[poisson] Contrat de poisson");
        c.setAmount(25);
        c.setProducer("Ordralfabétix");
        contractDao.create(c);

        String subId = subscriber.getId();
        Long contId = c.getId();

        subscribers = dao.findForContract(contId);
        Assertions.assertThat(subscribers).isEmpty();

        Assertions.assertThat(subscriber.numberOfSubscriptions()).isZero();

        subscriber.subscribe(c);
        subscriber = dao.update(subscriber);

        //Test that the number of subscriber is valid before saving to DB and after reloading entity :
        Assertions.assertThat(subscriber.numberOfSubscriptions()).isEqualTo(1);
        detach(subscriber);  //Remove entity from current session bag
        subscriber = dao.read(subId);
        Assertions.assertThat(subscriber.numberOfSubscriptions()).isEqualTo(1);


        Contract c2 = new Contract();
        c2.setTitle("[viande] Contrat de viande");
        c2.setProducer("Le fils du boucher");
        c2.setAmount(50);
        contractDao.create(c2);

        Assert.assertTrue(subscriber.isSubscribedTo(c));
        Assert.assertFalse(subscriber.isSubscribedTo(c2));

        Subscriber subscriber2 = new Subscriber();
        subscriber2.setId("2");
        subscriber2.setName("totofsd");
        subscriber2.setSurname("tfditi");
        subscriber2.setAddress("toto@machfsin.fr");
        subscriber2.subscribe(c);
        dao.create(subscriber2);

        detach(subscriber, subscriber2);

        subscribers = dao.findForContract(contId);
        Assertions.assertThat(subscribers).hasSize(2);
    }


    @Test(expected = java.lang.AssertionError.class)
    public void testContracts2() {

        Subscriber subscriber = new Subscriber();
        subscriber.setId("1");
        subscriber.setName("toto");
        subscriber.setSurname("titi");
        subscriber.setAddress("toto@machin.fr");

        Contract c = new Contract();
        c.setTitle("[poisson] Contrat de poisson");
        c.setProducer("Ordralfabétix");
        c.setAmount(25);

        subscriber.subscribe(c);
    }

    @Test
    public void testSeveralSubscriptions() {
        SubscriberDao subscriberDao = this.getSubscriberDao();
        ContractDao contractDao = this.getContractDao();

        Subscriber subscriber1 = new Subscriber();
        subscriber1.setId("1");
        subscriber1.setName("toto");
        subscriber1.setSurname("titi");
        subscriber1.setAddress("toto@machin.fr");

        Subscriber subscriber2 = new Subscriber();
        subscriber2.setId("2");
        subscriber2.setName("fdsf");
        subscriber2.setSurname("fds");
        subscriber2.setAddress("fdsf@fsd.fr");

        Contract c1 = new Contract();
        c1.setTitle("[poisson] Contrat de poisson");
        c1.setProducer("Ordralfabétix");
        c1.setAmount(25);

        Contract c2 = new Contract();
        c2.setTitle("[poisson] Contrat de poisson 2");
        c2.setProducer("La femme de Ordralfabétix");
        c2.setAmount(30);

        persist(c1, c2, subscriber1, subscriber2);

        Assertions.assertThat(subscriberDao.countForContract(c1.getId())).isZero();
        Assertions.assertThat(subscriberDao.countForContract(c2.getId())).isZero();

        subscriber1.subscribe(c1);
        subscriber2.subscribe(c2);

        subscriberDao.update(subscriber1);
        subscriberDao.update(subscriber2);

        detach(subscriber1,subscriber2);

        subscriber1 = subscriberDao.read(subscriber1.getId());
        subscriber2 = subscriberDao.read(subscriber2.getId());

        Assert.assertTrue(subscriber1.isSubscribedTo(c1));
        Assert.assertTrue(subscriber2.isSubscribedTo(c2));

        Assert.assertFalse(subscriber1.isSubscribedTo(c2));
        Assert.assertFalse(subscriber2.isSubscribedTo(c1));

        Assertions.assertThat(subscriberDao.countForContract(c1.getId())).isEqualTo(1);
        Assertions.assertThat(subscriberDao.countForContract(c2.getId())).isEqualTo(1);

        contractDao.delete(c1);

        Assertions.assertThat(subscriberDao.countForContract(c1.getId())).isZero();
        Assertions.assertThat(subscriberDao.countForContract(42)).isZero();
    }

    private SubscriberDao getSubscriberDao() {
        return this.injector.getInstance(SubscriberDao.class);
    }

    private ContractDao getContractDao() {
        return this.injector.getInstance(ContractDao.class);
    }

}
