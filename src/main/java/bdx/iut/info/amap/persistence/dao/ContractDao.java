package bdx.iut.info.amap.persistence.dao;

import bdx.iut.info.amap.persistence.domain.Contract;

import java.util.List;

public interface ContractDao  {
    /**
     * Creates a contract
     *
     * @param entity contract to create
     */

    public void create(final Contract entity);

    /**
     * Read a contract by its identifier
     *
     * @param id identifier of contract to retrieve
     * @return Contract or null if not found
     */
    public Contract read(final long id);

    /**
     * Updates a contract
     *
     * @param entity contract to update
     * @return the updated contract entity
     */
    public Contract update(final Contract entity);

    /**
     * Deletes a contract
     *
     * @param entity contract to delete
     */
    public void delete(final Contract entity);


    /**
     * Retrieves all contracts
     *
     * @return contract list (cannot be null)
     */
    public List<Contract> findAll();

    /**
     * Find contracts by a keyword on title field
     *
     * @param keyword keyword
     * @return contracts with a title that match with the given keyword
     */
    public List<Contract> findByKeyword(final String keyword);
}
