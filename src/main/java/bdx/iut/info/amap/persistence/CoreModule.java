package bdx.iut.info.amap.persistence;

import bdx.iut.info.amap.persistence.dao.ContractDao;
import bdx.iut.info.amap.persistence.dao.SubscriberDao;
import bdx.iut.info.amap.persistence.dao.impl.ContractDaoImpl;
import bdx.iut.info.amap.persistence.dao.impl.SubscriberDaoImpl;
import com.google.inject.AbstractModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by rgiot on 06/02/17.
 *
 * Binds the objets necerray for the DAO stuff.
 * To ease the work, we do not work with inmplementations/interfaces
 */
public class CoreModule extends AbstractModule {
    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(CoreModule.class);

    @Override
    protected void configure() {
        logger.info("CoreModule configuration started...");

        logger.info("Install Contract Dao");
        bind(ContractDao.class).to(ContractDaoImpl.class);

        //logger.info("Install InstructionDao");
        //bind(InstructionDao.class);

        ///logger.info("Install Contract IngredientQuantityDao");
        //bind(IngredientQuantityDao.class);

        logger.info("Install Contract SubscriberDao");
        bind(SubscriberDao.class).to(SubscriberDaoImpl.class);

        logger.info("CoreModule configuration ended.");
    }
}
