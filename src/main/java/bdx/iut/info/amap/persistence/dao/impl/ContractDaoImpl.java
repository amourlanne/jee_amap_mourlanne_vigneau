package bdx.iut.info.amap.persistence.dao.impl;

import bdx.iut.info.amap.persistence.dao.ContractDao;
import bdx.iut.info.amap.persistence.domain.Contract;
import bdx.iut.info.amap.persistence.domain.Contract_;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Singleton
public class ContractDaoImpl implements ContractDao {
    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(ContractDaoImpl.class);

    /**
     * Injected Entity Manager
     */
    @Inject
    private Provider<EntityManager> entityManager;

    /**
     * Creates a contract
     *
     * @param entity contract to create
     */
    @Transactional
    public void create(final Contract entity) {
        logger.info("Try to save " + toString());
        logger.info("Entity Manager" + this.entityManager)
        ;
        logger.info("Entity Manager.get" + this.entityManager.get());
        this.entityManager.get().persist(entity);
        logger.info("Saved as " + entity.getId());
    }

    /**
     * Read a contract by its identifier
     *
     * @param id identifier of contract to retrieve
     * @return Contract or null if not found
     */
    public Contract read(final long id) {
        logger.debug("Read contract with id '{}'", id);
        final Contract entity = this.entityManager.get().find(Contract.class, id);
        if (entity == null) {
            logger.debug("Contract with id '{}' not found", id);
        } else {
            logger.debug("Contract with id '{}' found", id);
        }
        return entity;
    }

    /**
     * Updates a contract
     *
     * @param entity contract to update
     * @return the updated contract entity
     */
    @Transactional
    public Contract update(final Contract entity) {
        if (entity != null) {
            logger.debug("Update contract with id '{}'", entity.getId());
        }
        return this.entityManager.get().merge(entity);
    }

    /**
     * Deletes a contract
     *
     * @param entity contract to delete
     */
    @Transactional
    public void delete(final Contract entity) {
        //remove join table entries first, as contract has no idea about it so persistence engine cannot remove association entries
        Query sqlQuery = this.entityManager.get().createNativeQuery("delete from subscribers_contracts where contract_id = :id");
        sqlQuery.setParameter("id", entity.getId());
        sqlQuery.executeUpdate();

        this.entityManager.get().remove(entity);
    }


    /**
     * Retrieves all contracts
     *
     * @return contract list (cannot be null)
     */
    public List<Contract> findAll() {
        logger.debug("Retrieves all contracts");
        StringBuilder query = new StringBuilder("from ").append(Contract.class.getName());
        final List<Contract> contracts = this.entityManager.get().createQuery(query.toString()).getResultList();
        if (contracts.size() == 0) {
            logger.debug("no contract found");
        } else {
            logger.debug("{} contracts found", contracts.size());
        }
        return contracts;
    }

    /**
     * Find contracts by a keyword on title field
     *
     * @param keyword keyword
     * @return contracts with a title that match with the given keyword
     */
    public List<Contract> findByKeyword(final String keyword) {

        StringBuilder query = new StringBuilder("from ");
        query.append(Contract.class.getName());
        query.append(" as contract where lower(contract." + Contract_.title.getName() + ") like :name ");
        return this.entityManager.get()
                .createQuery(query.toString())
                .setParameter("name", "%" + keyword.toLowerCase() + "%")
                .getResultList();

    }
}