package bdx.iut.info.amap.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by rgiot on 12/02/17.
 */
public class IsContractValidator implements ConstraintValidator<IsContract, String> {
    public static final Pattern p = Pattern.compile("\\[(poisson|fruit|viande|légume|divers)\\] Contrat .*");

    @Override
    public void initialize(final IsContract isMajor) {

    }

    /**
     * A valid string starts by an uppercase character and ends by a dot
     */
    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext constraintValidatorContext) {

        Matcher m = p.matcher(value);

        return m.matches() ;
    }
}
