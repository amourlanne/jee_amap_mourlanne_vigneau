package bdx.iut.info.amap.web.servlet;

import bdx.iut.info.amap.persistence.dao.ContractDao;
import bdx.iut.info.amap.persistence.dao.SubscriberDao;
import bdx.iut.info.amap.persistence.domain.Contract;
import bdx.iut.info.amap.persistence.domain.Subscriber;
import bdx.iut.info.amap.web.servlet.model.subscriber.ContractVO;
import bdx.iut.info.amap.web.servlet.model.subscriber.SubscriberVO;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Singleton
public class SubscriberManageServlet extends HttpServlet {
    /**
     * constant for template location
     */
    private static final String BOOTFREE_TEMPLATE = "templates/main_subscriber.ftl";
    /**
     * constant for UTF-8 *
     */
    private static final String TEMPLATE_ENCODING = "UTF-8";
    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(SubscriberManageServlet.class);


    @Inject
    private ContractDao contractDao;

    @Inject
    private SubscriberDao subscriberDao;


    /**
     * Operate the administration actions depending on the request.
     * Better to have a controller pattern ...
     *
     * @param request
     */
    private void manageActions(HttpServletRequest request) {


    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

        // XXX Warning, there is a huge bug there anyone can show the place of anyone
        // Get the subscriber provided

        final String givenSubscriberId = request.getParameter("subscriberId");
        Subscriber subscriber = subscriberDao.read(givenSubscriberId);

        final String action = request.getParameter("action");
        if (null != action) {
            if (action.equals("modifySubscriber")) {
                String name = request.getParameter("subscriberName");
                String surname = request.getParameter("subscriberSurname");
                String address = request.getParameter("subscriberAddress");


                subscriber.setName(name);
                subscriber.setSurname(surname);
                subscriber.setAddress(address);

                subscriberDao.update(subscriber);
            } else if (action.equals("delContract")) {
                Contract c = contractDao.read(Long.parseLong(request.getParameter("contractId")));
                subscriber.unsubscribe(c);
                subscriberDao.update(subscriber);
            } else if (action.equals("subscribeContract")) {
                Contract c = contractDao.read(Long.parseLong(request.getParameter("contractId")));
                subscriber.subscribe(c);
                subscriberDao.update(subscriber);
            }
        }


        final List<Contract> contracts = contractDao.findAll();
        final List<ContractVO> availableContractsVO = new ArrayList<>();
        for (Contract c : contracts) {
            if (!subscriber.isSubscribedTo(c)) {
                availableContractsVO.add(convert(c));
            }
        }
        final SubscriberVO subscribersVO = convert(subscriber);

        // Retreive objects to feed the template
        Map<String, Object> root = new HashMap<String, Object>();
        root.put("subscriber", subscribersVO);
        root.put("availableContracts", availableContractsVO);

        String username = "";
        boolean isconnect = false;
        try {
            username = request.getUserPrincipal().getName();
            isconnect = true;
        } catch (NullPointerException e ) {}

        root.put("username", username);
        root.put("isconnect",isconnect);

        // XXX Boilercode for templates to copy paste on all serveltes
        // Manage freemarker stuff
        Template freemarkerTemplate = null;
        freemarker.template.Configuration freemarkerConfiguration = new freemarker.template.Configuration();
        freemarkerConfiguration.setClassForTemplateLoading(this.getClass(), "/");
        freemarkerConfiguration.setObjectWrapper(new DefaultObjectWrapper());
        try {
            freemarkerTemplate = freemarkerConfiguration.getTemplate(BOOTFREE_TEMPLATE, TEMPLATE_ENCODING);

        } catch (IOException e) {
            logger.error("Unable to process request, error during freemarker template retrieval.", e);

        }

        // navigation data and links
        root.put("title", "Java EE - AMAP Manager - v0.1 - Romain Giot");

        response.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        assert freemarkerTemplate != null;
        try {
            freemarkerTemplate.process(root, out);
            out.close();
        } catch (TemplateException e) {
            logger.error("Error during template processing", e);
        }

        // set mime type
        response.setContentType("text/html");

    }

    private ContractVO convert(final Contract contract) {
        ContractVO vo = new ContractVO();
        vo.setId(contract.getId());
        vo.setAmount(contract.getAmount());
        vo.setProducer(contract.getProducer());
        vo.setTitle(contract.getTitle());
        return vo;
    }

    private SubscriberVO convert(final Subscriber subscriber) {
        SubscriberVO vo = new SubscriberVO();
        vo.setId(subscriber.getId());
        vo.setName(subscriber.getName());
        vo.setAddress(subscriber.getAddress());
        vo.setSurname(subscriber.getSurname());
        for (Contract contract : subscriber.getContracts()) {
            ContractVO cVO = new ContractVO();
            cVO.setId(contract.getId());
            cVO.setAmount(contract.getAmount());
            cVO.setProducer(contract.getProducer());
            cVO.setTitle(contract.getTitle());
            vo.getContracts().add(cVO);
        }
        return vo;
    }
}
