package bdx.iut.info.amap.web.servlet;

import bdx.iut.info.amap.persistence.dao.ContractDao;
import bdx.iut.info.amap.persistence.dao.SubscriberDao;
import bdx.iut.info.amap.persistence.domain.Contract;
import bdx.iut.info.amap.persistence.domain.Subscriber;
import bdx.iut.info.amap.web.servlet.model.admin.ContractVO;
import bdx.iut.info.amap.web.servlet.model.admin.SubscriberVO;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Singleton
public class ManagerContractServlet extends HttpServlet {
    private static final String BOOTFREE_TEMPLATE = "templates/contract_view.ftl";
    private static final String TEMPLATE_ENCODING = "UTF-8";
    private static final Logger logger = LoggerFactory.getLogger(ManagerContractServlet.class);

    @Inject
    private ContractDao contractDao;

    @Inject
    private SubscriberDao subscriberDao;

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

        Map<String, Object> root = new HashMap<String, Object>();


        String username = "";
        boolean isconnect = false;
        try {
            username = request.getUserPrincipal().getName();
            isconnect = true;
        } catch (NullPointerException e ) {}

        root.put("username", username);
        root.put("isconnect",isconnect);

        final long givenContractId = Long.parseLong(request.getParameter("contractId"));
        Contract contract = contractDao.read(givenContractId);

        final String action = request.getParameter("action");
        if (null != action) {
            if (action.equals("delContract")) {
                Subscriber subscriber = subscriberDao.read(request.getParameter("subscriberId"));
                subscriber.unsubscribe(contract);
                subscriberDao.update(subscriber);
            }
        }

        ContractVO contractVO = convert(contract);
        root.put("contract", contractVO);
        root.put("revenue",contractVO.getNumberOfSubscribers() * contractVO.getAmount());

        final List<Subscriber> subscribers = subscriberDao.findAll();

        final List<SubscriberVO> submitedSubscribersVO = new ArrayList<>();

        for (Subscriber subscriber : subscribers) {
            if (subscriber.isSubscribedTo(contract)) {
                submitedSubscribersVO.add(convert(subscriber));
            }
        }
        root.put("subscribers",submitedSubscribersVO);
        
        Template freemarkerTemplate = null;
        freemarker.template.Configuration freemarkerConfiguration = new freemarker.template.Configuration();
        freemarkerConfiguration.setClassForTemplateLoading(this.getClass(), "/");
        freemarkerConfiguration.setObjectWrapper(new DefaultObjectWrapper());
        try {
            freemarkerTemplate = freemarkerConfiguration.getTemplate(BOOTFREE_TEMPLATE, TEMPLATE_ENCODING);

        } catch (IOException e) {
            logger.error("Unable to process request, error during freemarker template retrieval.", e);

        }

        // navigation data and links
        root.put("title", "Page contrat");

        response.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        assert freemarkerTemplate != null;
        try {
            freemarkerTemplate.process(root, out);
            out.close();
        } catch (TemplateException e) {
            logger.error("Error during template processing", e);
        }

        // set mime type
        response.setContentType("text/html");

    }
    private ContractVO convert(final Contract contract) {
        ContractVO vo = new ContractVO();
        vo.setId(contract.getId());
        vo.setAmount(contract.getAmount());
        vo.setProducer(contract.getProducer());
        vo.setTitle(contract.getTitle());
        vo.setNumberOfSubscribers(subscriberDao.countForContract(contract.getId()));
        return vo;
    }
    private SubscriberVO convert(final Subscriber subscriber) {
        SubscriberVO vo = new SubscriberVO();
        vo.setId(subscriber.getId());
        vo.setName(subscriber.getName());
        vo.setAddress(subscriber.getAddress());
        vo.setSurname(subscriber.getSurname());
        vo.setNumberOfSubscriptions(subscriber.numberOfSubscriptions());
        vo.setSubscriptionsAmount(subscriber.subscriptionsAmount());
        return vo;
    }
}
