package bdx.iut.info.amap.web.servlet;

import bdx.iut.info.amap.persistence.dao.ContractDao;
import bdx.iut.info.amap.persistence.dao.SubscriberDao;
import bdx.iut.info.amap.persistence.domain.Contract;
import bdx.iut.info.amap.persistence.domain.Subscriber;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Simple servlet which fills the database with dummy stuffs
 */
@Singleton
public class InitServlet extends HttpServlet {
    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(InitServlet.class);

    /**
     * Injected contract DAO
     */
    @Inject
    private ContractDao contractDao;


    /**
     * Injected subscriber DAO
     */
    @Inject
    private SubscriberDao subscriberDao;

    /**
     * HTTP GET access
     *
     * @param req  use an optional nb parameter to make evidence of transactionnal behavior volontary triggering an exception
     * @param resp response to sent
     * @throws ServletException by container
     * @throws IOException      by container
     */

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {

        BufferedReader reader = req.getReader();

        resp.setContentType("text/html; charset=UTF-8");
        PrintWriter out = new PrintWriter(new OutputStreamWriter(resp.getOutputStream(), "UTF8"), true);

         if(contractDao.findAll().size() == 0 && subscriberDao.findAll().size() == 0) {
            try {
                ArrayList<Contract> contracts = new ArrayList<Contract>();


                Contract legumes = new Contract();
                legumes.setProducer("Mme Laurence Lavoie");
                legumes.setTitle("[légume] Contrat légumes frais");
                legumes.setAmount(4 * 6);


                Contract pain = new Contract();
                pain.setProducer("Mr Guerin Larivière");
                pain.setTitle("[divers] Contrat pain");
                pain.setAmount(4 * 9);

                Contract oeuf = new Contract();
                oeuf.setProducer("Mr Davet Chalut");
                oeuf.setTitle("[divers] Contrat oeuf frais");
                oeuf.setAmount(4 * 5);

                Contract poisson = new Contract();
                poisson.setProducer("Mr Hugues Bordeaux");
                poisson.setTitle("[poisson] Contrat poisson");
                poisson.setAmount(4 * 8);

                Contract fruit = new Contract();
                fruit.setProducer("Mr Onfroi Aubé");
                fruit.setTitle("[fruit] Contrat fruits rouge");
                fruit.setAmount(4 * 2);

                Contract viande2 = new Contract();
                viande2.setProducer("Mr Ranger Hachée");
                viande2.setTitle("[viande] Contrat viande de boeuf");
                viande2.setAmount(4 * 5);

                Contract viande = new Contract();
                viande.setProducer("Mme Elita Mathieu");
                viande.setTitle("[viande] Contrat findus steak de cheval");
                viande.setAmount(4 * 3);



                contractDao.create(legumes);
                contractDao.create(pain);
                contractDao.create(oeuf);
                contractDao.create(viande2);
                contractDao.create(viande);
                contractDao.create(fruit);
                contractDao.create(poisson);


                Subscriber toto = new Subscriber();
                toto.setId("amapuser1");
                toto.setName("Lamare");
                toto.setSurname("Faustin ");
                toto.setAddress("19, rue Clement Marot 93380 PIERREFITTE-SUR-SEINE");

                Subscriber amapuser3  = new Subscriber();
                amapuser3.setId("amapuser3");
                amapuser3.setName("Gaudreau");
                amapuser3.setSurname("Albracca");
                amapuser3.setAddress("53, rue du Paillle en queue 92300 LEVALLOIS-PERRET");

                Subscriber amapuser4  = new Subscriber();
                amapuser4.setId("amapuser4");
                amapuser4.setName("Croquetaigne");
                amapuser4.setSurname("Clothilde");
                amapuser4.setAddress("25, rue Beauvau 13002 MARSEILLE");

                Subscriber amapuser5  = new Subscriber();
                amapuser5.setId("amapuser5");
                amapuser5.setName("Gougeon");
                amapuser5.setSurname("Blanche");
                amapuser5.setAddress("14, rue Michel Ange 76600 LE HAVRE");

                Subscriber amapuser6  = new Subscriber();
                amapuser6.setId("amapuser6");
                amapuser6.setName("Cartier");
                amapuser6.setSurname("Rabican");
                amapuser6.setAddress("3, avenue Jean Portalis 93290 TREMBLAY-EN-FRANCE");

                Subscriber amapuser7  = new Subscriber();
                amapuser7.setId("amapuser7");
                amapuser7.setName("Lajeunesse");
                amapuser7.setSurname("Caresse");
                amapuser7.setAddress("2, rue Marie de Médicis 31700 BLAGNAC");

                amapuser3.subscribe(pain);
                amapuser3.subscribe(legumes);
                amapuser3.subscribe(viande2);

                subscriberDao.create(amapuser3);

                amapuser4.subscribe(oeuf);
                amapuser4.subscribe(pain);

                subscriberDao.create(amapuser4);

                amapuser5.subscribe(poisson);
                amapuser5.subscribe(legumes);
                amapuser5.subscribe(viande);

                subscriberDao.create(amapuser5);

                amapuser6.subscribe(legumes);

                subscriberDao.create(amapuser6);

                amapuser7.subscribe(poisson);
                amapuser7.subscribe(fruit);

                subscriberDao.create(amapuser7);

                subscriberDao.create(toto);
                toto.subscribe(legumes);
                toto.subscribe(pain);
                toto = subscriberDao.update(toto);


                Subscriber tutu = new Subscriber();
                tutu.setId("amapuser2");
                tutu.setName("Berie");
                tutu.setSurname("Denise ");
                tutu.setAddress("41, Rue Roussy\n" +
                        "84100 ORANGE");

                subscriberDao.create(tutu);
                tutu.subscribe(legumes);
                tutu.subscribe(pain);
                tutu.subscribe(oeuf);
                tutu.subscribe(poisson);
                tutu.subscribe(fruit);
                tutu.subscribe(viande);
                tutu.subscribe(viande2);
                tutu = subscriberDao.update(tutu);

            } catch (org.hibernate.exception.ConstraintViolationException e) {

                out.print("<b>ERROR - I guess it is because everythn is already in memory</b>");
                out.print(e);
            }
         }
        resp.sendRedirect("home");
    }
}
