package bdx.iut.info.amap.web.servlet;

import bdx.iut.info.amap.persistence.dao.ContractDao;
import bdx.iut.info.amap.persistence.dao.SubscriberDao;
import bdx.iut.info.amap.persistence.domain.Contract;
import bdx.iut.info.amap.persistence.domain.Subscriber;
import bdx.iut.info.amap.web.servlet.model.admin.ContractVO;
import bdx.iut.info.amap.web.servlet.model.admin.SubscriberVO;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Singleton
public class HomeServlet extends HttpServlet {

    private static final String BOOTFREE_TEMPLATE = "templates/home.ftl";
    private static final String TEMPLATE_ENCODING = "UTF-8";
    private static final Logger logger = LoggerFactory.getLogger(HomeServlet.class);

    @Inject
    private ContractDao contractDao;
    @Inject
    private SubscriberDao subscriberDao;

    public HomeServlet() {
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

        Map<String, Object> root = new HashMap<String, Object>(); // Retreive objects to feed the template

        final List<Contract> contracts = contractDao.findAll();
        final List<ContractVO> contractsVO = new ArrayList<>();
        for (Contract contract : contracts) {
            contractsVO.add(convert(contract));
        }
        final List<Subscriber> subscribers = subscriberDao.findAll();
        final List<SubscriberVO> subscribersVO = new ArrayList<>();
        for (Subscriber subscriber : subscribers) {
            subscribersVO.add(convert(subscriber));
        }

        Template freemarkerTemplate = null;
        freemarker.template.Configuration freemarkerConfiguration = new freemarker.template.Configuration();
        freemarkerConfiguration.setClassForTemplateLoading(this.getClass(), "/");
        freemarkerConfiguration.setObjectWrapper(new DefaultObjectWrapper());
        try {
            freemarkerTemplate = freemarkerConfiguration.getTemplate(BOOTFREE_TEMPLATE, TEMPLATE_ENCODING);
        } catch (IOException e) {
            logger.error("Unable to process request, error during freemarker template retrieval.", e);
        }

        String username = "";
        boolean isconnect = false;
        try {
            username = request.getUserPrincipal().getName();
            isconnect = true;
        } catch (NullPointerException e ) {}
        boolean isinit = !(contractDao.findAll().size() == 0 && subscriberDao.findAll().size() == 0);

        root.put("username", username);
        root.put("isconnect",isconnect);
        root.put("isinit",isinit);

        root.put("title", "Home");
        root.put("contracts", contractsVO);
        root.put("subscribers",subscribersVO);

        root.put("numberSubscriber", subscribersVO.size());

        response.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        assert freemarkerTemplate != null;
        try {
            freemarkerTemplate.process(root, out);
            out.close();
        } catch (TemplateException e) {
            logger.error("Error during template processing", e);
        }

        // set mime type
        response.setContentType("text/html");
    }

    private ContractVO convert(final Contract contract) {
        ContractVO vo = new ContractVO();
        vo.setId(contract.getId());
        vo.setAmount(contract.getAmount());
        vo.setProducer(contract.getProducer());
        vo.setTitle(contract.getTitle());
        vo.setNumberOfSubscribers(subscriberDao.countForContract(contract.getId()));
        return vo;
    }
    private SubscriberVO convert(final Subscriber subscriber) {
        SubscriberVO vo = new SubscriberVO();
        vo.setId(subscriber.getId());
        vo.setName(subscriber.getName());
        vo.setAddress(subscriber.getAddress());
        vo.setSurname(subscriber.getSurname());
        vo.setNumberOfSubscriptions(subscriber.numberOfSubscriptions());
        vo.setSubscriptionsAmount(subscriber.subscriptionsAmount());
        return vo;
    }
}