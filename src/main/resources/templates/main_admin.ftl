<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>${title}</title>
    <!-- Bootstrap core CSS-->
    <link href="/amap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="/amap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="/amap/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="/amap/css/sb-admin.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="index.html">Amap</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
                <a class="nav-link" href="/amap/home">
                    <span class="nav-link-text">Home</span>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav sidenav-toggler">
            <li class="nav-item">
                <a class="nav-link text-center" id="sidenavToggler">
                    <i class="fa fa-fw fa-angle-left"></i>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link">
                <#if isconnect>
                    Welcome ${username}
                </#if>
                </a>
            </li>
        </ul>
    </div>
</nav>


<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="">Amap</a>
            </li>
            <li class="breadcrumb-item">
                <a href="#">Manager</a>
            </li>
            <li class="breadcrumb-item active">main</li>
        </ol>
        <#if iserror>
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    Action rejetée !
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
        </#if>
        <div class="row" style="margin-bottom: 20px;">
            <div class="col-xl-6 col-sm-6 mb-6">
                <div class="card text-white bg-secondary o-hidden h-100">
                    <div class="card-body">
                        <div class="card-body-icon">
                            <i class="fa fa-fw fa-list"></i>
                        </div>
                        <div class="mr-5">${numberSubscriber} subscribers !</div>
                    </div>
                    <a class="card-footer text-white clearfix small z-1" href="subscriber/add">
                        <span class="float-left">Add customer</span>
                        <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
                    </a>
                </div>
            </div>
            <div class="col-xl-6 col-sm-6 mb-6">
                <div class="card text-white bg-info o-hidden h-100">
                    <div class="card-body">
                        <div class="card-body-icon">
                            <i class="fa fa-fw fa-shopping-cart"></i>
                        </div>
                        <div class="mr-5">${numberContract} contracts !</div>
                    </div>
                    <a class="card-footer text-white clearfix small z-1" href="contract/add">
                        <span class="float-left">Add contract</span>
                        <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-table"></i> Contract list</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Producer</th>
                            <th>Amount</th>
                            <th>Subscribers</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                                    <#list contracts as contract>
                                    <tr>
                                        <td><a href="contract/view?contractId=${contract.getId()}">${contract.getTitle()}</a></td>
                                        <td>${contract.getProducer()}</td>
                                        <td>${contract.getAmount()}</td>
                                        <td>${contract.getNumberOfSubscribers()}</td>
                                        <td>
                                            <form>
                                                <input name="contractId" value="${contract.getId()}" type="hidden"/>
                                                <input name="action" value="delContract"  type="hidden"/>
                                                <input class="btn btn-danger btn-block" type="submit" value="Delete" />
                                            </form>
                                        </td>
                                    </tr>
                                    </#list>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-table"></i> Subscriber list</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Surname</th>
                            <th>Address</th>
                            <th>Number of contracts</th>
                            <th>Amount of contracts</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                <#list subscribers as subscriber>
                <tr>
                    <td>${subscriber.getName()}</td>
                    <td>${subscriber.getSurname()}</td>
                    <td>${subscriber.getAddress()!}</td>
                    <td>${subscriber.getNumberOfSubscriptions()}</td>
                    <td>${subscriber.getSubscriptionsAmount()}</td>
                    <td>
                        <form>
                            <input name="subscriberId" value="${subscriber.getId()}" type="hidden"/>
                            <input name="action" value="delSubscriber" type="hidden" />
                            <input type="submit" class="btn btn-danger btn-block" value="Delete"/>
                        </form>
                    </td>
                </tr>
                </#list>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fa fa-pie-chart"></i> Subscription Chart</div>
                    <div class="card-body">
                        <canvas id="myPieChart" width="100%" height="100"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-8">
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fa fa-bar-chart"></i> Revenue Chart</div>
                    <div class="card-body">
                        <div class="row">
                                <canvas id="myBarChart" width="100" height="50"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="sticky-footer">
        <div class="container">
            <div class="text-center">
                <small>Copyright © Amap 2018</small>
            </div>
        </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <!-- Bootstrap core JavaScript-->
    <script src="/amap/vendor/jquery/jquery.min.js"></script>
    <script src="/amap/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="/amap/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="/amap/vendor/chart.js/Chart.min.js"></script>
    <script src="/amap/vendor/datatables/jquery.dataTables.js"></script>
    <script src="/amap/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="/amap/js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="/amap/js/sb-admin-datatables.min.js"></script>
    <script src="/amap/js/sb-admin-charts.min.js"></script>
    <script>

        var ctx = document.getElementById("myBarChart");

        var data = {
            labels: [],
            datasets: [{
                label: "Revenue",
                backgroundColor: "rgba(2,117,216,1)",
                borderColor: "rgba(2,117,216,1)",
                data: [],
            }],
        };

        var numberContract = ${numberContract};

        var maxrevenue = 0;

        <#list contracts as contract>

        data.labels.push("${contract.getTitle()}");
        var amount = ${contract.getAmount()};
        var number = ${contract.getNumberOfSubscribers()};
        var revenue = number*amount;
        if (revenue > maxrevenue) maxrevenue = revenue;
            data.datasets[0].data.push(revenue);
        </#list>

        var myLineChart = new Chart(ctx, {
            type: 'bar',
            data: data,
            options: {
                scales: {
                    xAxes: [{
                        time: {
                            unit: 'month'
                        },
                        gridLines: {
                            display: false
                        },
                        ticks: {
                            maxTicksLimit: numberContract
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            min: 0,
                            max: maxrevenue,
                            maxTicksLimit: 5
                        },
                        gridLines: {
                            display: true
                        }
                    }],
                },
                legend: {
                    display: false
                }
            }
        });

        data = {
            labels: [],
            datasets: [{
                data: [],
                backgroundColor: [],
            }]
        };
        var numberOfSubscriber = ${numberSubscriber};

                <#list contracts as contract>
            data.labels.push("${contract.getTitle()}");
            var number = ${contract.getNumberOfSubscribers()};
            data.datasets[0].data.push(number/numberOfSubscriber);
            data.datasets[0].backgroundColor.push(getRandomColor());
                </#list>


        ctx = document.getElementById("myPieChart");
        var myPieChart = new Chart(ctx, {
            type: 'pie',
            data: data
        });

        function getRandomColor() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }
    </script>
</div>
</body>

</html>
