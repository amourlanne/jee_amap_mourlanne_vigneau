<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>${title}</title>
    <!-- Bootstrap core CSS-->
    <link href="/amap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="/amap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="/amap/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="/amap/css/sb-admin.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="../../home">Amap</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
                <a class="nav-link" href="../../home">
                    <span class="nav-link-text">Home</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
                <a class="nav-link" href="../main">
                    <span class="nav-link-text">Manager panel</span>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav sidenav-toggler">
            <li class="nav-item">
                <a class="nav-link text-center" id="sidenavToggler">
                    <i class="fa fa-fw fa-angle-left"></i>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link">
                <#if isconnect>
                    Welcome ${username}
                </#if>
                </a>
            </li>
        </ul>
    </div>
</nav>


<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="">Amap</a>
            </li>
            <li class="breadcrumb-item">
                <a href="#">Manager</a>
            </li>
        <li class="breadcrumb-item active">ADD Contract</li>
        </ol>
        <form action="../main">
            <div class="form-group">
                <label>Contract title</label>
                <input class="form-control" name="contractName" placeholder="De la forme : [poisson|fruit|viande|légume|divers] Contrat ..." pattern="\[(poisson|fruit|viande|légume|divers)\] Contrat .*"/>
            </div>
            <div class="form-group">
                <label>Producer</label>
                <input class="form-control" name="contractProducer" />
            </div>
            <div class="form-group">
                <label>Amount</label>
                <input class="form-control" name="contractAmount" type="number"/>
            </div>

            <div class="form-group">
                <input name="action" value="addContract" type="hidden" />
                <input type="submit" class="btn btn-success btn-block" value="Ajouter" />
            </div>
        </form>
    </div>
    <footer class="sticky-footer">
        <div class="container">
            <div class="text-center">
                <small>Copyright © Amap 2018</small>
            </div>
        </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <!-- Bootstrap core JavaScript-->
    <script src="/amap/vendor/jquery/jquery.min.js"></script>
    <script src="/amap/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="/amap/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="/amap/vendor/chart.js/Chart.min.js"></script>
    <script src="/amap/vendor/datatables/jquery.dataTables.js"></script>
    <script src="/amap/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="/amap/js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="/amap/js/sb-admin-datatables.min.js"></script>
    <script src="/amap/js/sb-admin-charts.min.js"></script>
</div>
</body>

</html>
